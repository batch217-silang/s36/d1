// ALL ENDPOINTS
const express = require("express");
const router = express.Router();
const taskController = require("../controllers/taskController.js");

/**************************************************************
[SECTION] ROUTES
= responsible for defining or creating endpoints
= all business logics are done in the controller
*/

router.get("/viewTasks", (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})

router.post("/addNewTask", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});


router.delete("/deleteTask/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => {
		res.send(resultFromController)
	});
})


router.put("/updateTask/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})



// [S36 ACTIVITY CODE] 
// specific task: Attend bootcamp = code WORKING!
router.get("/specificTask/:id", (req, res) => {
	taskController.getSpecificTask(req.params.id, req.status).then(resultFromController => res.send(resultFromController));
})

// update status to complete = code NOT WORKING!
router.put("/updateStatus/:id", (req, res) => {
	taskController.updateStatus(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})


module.exports = router;

/**************************************************************
[NOTES] 
module = is resposible for transferring
params = the id is directly added on the url


For your s36 activity po, check mo kung same yung function name po sa updating a status. Then dun naman po sa specific task, gamit ka po ng findById. Thanks po!

*/



