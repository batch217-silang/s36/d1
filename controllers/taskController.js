const Task = require("../models/task.js");


module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	});
}


module.exports.createTask = (requestBody) => {
	let newTask =  new Task({
		name: requestBody.name
		})
	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
			return false;
		}else{
			return task;
		}
	})
}


module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removeTask, err) => {
		if(err){
			console.log(err); 
			return false;
		}else{
			return removeTask;
		}
	})
}


module.exports.updateTask = (taskId, newContent) =>{
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}

		result.name = newContent.name;
		return result.save().then((updateTask, saveErr) => {
			if(saveErr){
				console.log(saveErr)
				return false;
			}else{
				return updateTask;
			}
		})
	})
}


// [S36 ACTIVITY CODE] 
// specific task: Attend bootcamp = code WORKING!
module.exports.getSpecificTask = (taskId, statusContent) => {
	return Task.findById(taskId).then((result, error) => {
		return result;
	});
}


// update status to complete = code WORKING!
module.exports.updateStatus = (taskId, newStatus) =>{
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}

		result.status = newStatus.status;
		return result.save().then((updateStatus, saveErr) => {
			if(saveErr){
				console.log(saveErr)
				return false;
			}else{
				return updateStatus;
			}
		})
	})
}

