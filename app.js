// SET UP DEPENDENCIES/MODULES
const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require ("./routes/taskRoute.js")


// SERVER SET UP
const app = express();
const port = 3001


// MIDDLEWARES
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use("/tasks", taskRoute);


// DB CONNECTION
mongoose.connect("mongodb+srv://admin:admin123@zuitt.25fvemh.mongodb.net/B217_to-do?retryWrites=true&w=majority", {
	useNewUrlParser : true,
	useUnifiedTopology : true
});

app.listen(port, () => console.log(`Now listening to port ${port}`));

// localhost:3001/tasks/viewTasks
// localhost:3001/tasks/addNewTask

